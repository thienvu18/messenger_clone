import { createStackNavigator } from 'react-navigation';
import { Platform } from 'react-native';

import SplashScreen from './screens/SplashScreen';
import CameraScreen from './screens/CameraScreen';
import MySearchBar from './components/SearchBar';
import { BottomTabNavigation } from './navigations/BottomTabNavigation';

const ModalStack = createStackNavigator(
  {
    MainScreen: {
      screen: BottomTabNavigation,
      navigationOptions: {
        gesturesEnabled: false,
        header: MySearchBar
      }
    },
    CameraScreen: {
      screen: CameraScreen,
      navigationOptions: {
        gesturesEnabled: false,
        header: null
      }
    }
  },
  {
    mode: Platform.OS === 'ios' ? 'modal' : 'card'
  }
);

export default (MessengerApp = createStackNavigator(
  {
    SplashScreen: {
      screen: SplashScreen,
      navigationOptions: {
        header: null
      }
    },
    MainScreen: {
      screen: ModalStack,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'MainScreen'
  }
));
