import { AppRegistry } from 'react-native';
import MessengerApp from './App';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => MessengerApp);
