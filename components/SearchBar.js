import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { SearchBar } from 'react-native-elements';

export default class MySearchBar extends Component {
  render() {
    if (Platform.OS === 'android') {
      return (
        <SearchBar
          round
          showLoading
          platform="android"
          cancelIcon={{ type: 'font-awesome', name: 'chevron-left' }}
          clearIcon={{ type: 'material-icons', name: 'clear' }}
          placeholder="Search"
        />
      );
    } else {
      return (
        <SearchBar
          round
          showLoading
          platform="ios"
          cancelButtonTitle="Cancel"
          placeholder="Search"
        />
      );
    }
  }
}
