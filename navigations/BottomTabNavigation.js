import { createBottomTabNavigator } from 'react-navigation';

import * as BottomTabbarIcons from '../icons/BottomTabbarIcons';
import { HomeTabNavigation } from './HomeTabNavigation';
import PeopleScreen from '../screens/PeopleScreen';
import CameraScreen from '../screens/CameraScreen';
import GameScreen from '../screens/GameScreen';

export const BottomTabNavigation = createBottomTabNavigator(
  {
    HomeScreen: {
      screen: HomeTabNavigation,
      navigationOptions: {
        header: null,
        tabBarIcon: BottomTabbarIcons.HomeTabIcon
      }
    },
    PeopleScreen: {
      screen: PeopleScreen,
      navigationOptions: {
        header: null,
        tabBarIcon: BottomTabbarIcons.PeopleTabIcon
      }
    },
    CameraTabScreen: {
      screen: CameraScreen,
      navigationOptions: ({ navigation }) => ({
        header: null,
        tabBarIcon: BottomTabbarIcons.CameraTabIcon,
        tabBarOnPress: ({ navigation }) => {
          navigation.navigate('CameraScreen');
        }
      })
    },
    GameScreen: {
      screen: GameScreen,
      navigationOptions: {
        header: null,
        tabBarIcon: BottomTabbarIcons.GameTabIcon
      }
    }
  },
  {
    tabBarOptions: {
      showLabel: false
    }
  }
);
