import { createMaterialTopTabNavigator } from 'react-navigation';

import MessageScreen from '../screens/MessageScreen';
import ActiveScreen from '../screens/ActiveScreen';
import GroupsScreen from '../screens/GroupsScreen';
import CallScreen from '../screens/CallScreen';

export const HomeTabNavigation = createMaterialTopTabNavigator(
  {
    MessageScreen: {
      screen: MessageScreen,
      navigationOptions: {
        header: null,
        title: 'Message'
      }
    },
    ActiveScreen: {
      screen: ActiveScreen,
      navigationOptions: {
        header: null,
        title: 'Active'
      }
    },
    GroupsScreen: {
      screen: GroupsScreen,
      navigationOptions: {
        header: null,
        title: 'Groups'
      }
    },
    CallScreen: {
      screen: CallScreen,
      navigationOptions: {
        header: null,
        title: 'Call'
      }
    }
  },
  {
    tabBarPosition: 'top'
  }
);
