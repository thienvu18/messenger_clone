import React, { Component } from 'react';

import Icon from 'react-native-vector-icons/MaterialIcons';

export const HomeTabIcon = <Icon name="home" size={30} color="#900" />;
export const PeopleTabIcon = <Icon name="people" size={30} color="#900" />;
export const CameraTabIcon = <Icon name="camera" size={30} color="#900" />;
export const GameTabIcon = <Icon name="games" size={30} color="#900" />;
